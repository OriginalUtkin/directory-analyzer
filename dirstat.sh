#!/bin/bash

###
#IOS, project 1
#File: dirstat.sh
#Author: xutkin00
###

export LC_ALL=C
#IFS=$(echo -en "\n\b")

print_help()
{
	echo "Usage: $0 dirstat[-i FILE_ERE] [DIR]
	       [DIR] - zadany adresar" >&2
	exit 1
}

Dir_work()
{	
	#1)vypis current DIR
	echo "Root directory: $orig"
	
	#2)pocet adresaru v DIR
	 adr_in_dir=`find . -type d ! -name "$file_ere"| wc -l` 
	echo "Directories: $adr_in_dir" 

	#Pripad kdyz pocet adresaru se rovna 0 
	#3)max hloubka adresar
	#Nacist adresare do pole seznam_adr.
	seznam_dir=`find . -type d ! -name "$file_ere"`
	max_depth=0
	for i in $seznam_dir
		do
			parse=`echo $i | sed s/[^/]//g | wc -c` #pocet symbolu "/" v ceste
			if [ "$parse" -gt "$max_depth" ]
				then 
					max_depth=$parse
				else
					continue
			fi
		done
		echo "Max depth: $max_depth"

	#4)Je aretmiticky prumer poctu obyc souboru v kazdem adresari
	count_dir=0 #pocet neprazdnych dir
	file_counter=0
	file_sum=0

	for i in $seznam_dir #pocet soubor
		do
			file_counter=`find $i -maxdepth 1 -type f ! -name "$file_ere"  | wc -l`
			file_sum=$(($file_sum+$file_counter))

	done

	all_files=$file_sum

	no_empty=`find .  -type d ! -name "$file_ere"`
	for i in $no_empty
		do
			if [ "$(find $i -maxdepth 1 -type f ! -name "$file_ere")" ];then
				count_dir=$(($count_dir+1))
			fi
		done
		if [ "$count_dir" -eq "0" ]
			then
				echo "Average no. of files: 0"
			else
				aver_t=` expr $all_files / $count_dir `
				echo "Average no. of files: $aver_t"
		fi

	#5)Pocet vsech souboru
	
	echo "All files: $all_files"

	#6)Nejvetsi soubor 
	cur_max_file_size=0
	for i in $seznam_dir
		do
			pocet_obj=`find $i -maxdepth 1 -type f  ! -name "$file_ere" -printf "%s\n" | sort -nr |wc -l` #pro uplne prazny adresar
			if [ "$pocet_obj" -eq "0" ]
				then
					continue
			fi
			max_file_size=`find $i -maxdepth 1 -type f ! -name "$file_ere" -printf "%s\n" | sort -nr | sed -n 1p` #hledani nejvetsiho souboru v adresari

			if [ "$max_file_size" -gt "$cur_max_file_size" ]
				then
					cur_max_file_size=$max_file_size
			fi
	done

	if [ "$all_files" -eq "0" ]
		then
			echo "  Largest file: N/A"
		else
			if [ "$cur_max_file_size" -eq "0" ]
				then
					echo "  Largest file: N/A"
				else	
					echo "  Largest file: $cur_max_file_size"
			fi
	fi

	#7)Average file size
	sum_file_size=0 #sum vseh  rozsahu souboru
	cur_sum_file_size=0
	
	for i in $seznam_dir
		do
			cur_sum_file_size=`find $i -maxdepth 1 -type f ! -name "$file_ere" -ls | awk '{sum = sum+$7};END{print sum}'` #sum vsech rozsahu souboru ve vsech adresarich
			sum_file_size=$(($cur_sum_file_size+$sum_file_size))
	done
	if [ "$all_files" -eq "0" ]
		then 
			echo "  Average file size: N/A"
		else 
			aver_f_size=` expr $sum_file_size / $all_files `
			echo "  Average file size: $aver_f_size" 
		fi

	#8)File size median
	all_file_size=`find . -type f ! -name "$file_ere" -printf "%s\n" | sort -nr`
	pocet_radku=`find . -type f ! -name "$file_ere" -printf "%s\n" | sort -nr | wc -l`

	ostatok=0
	result_del=0
	mediana=0
	median_count=1 #pro cyklus
	mezivysl=0 	   #pro ulozeni 1 podminky
	ost_mezivysl=0 #ostatok delenipro 2 podminku
	mediana_d1=0 #
	mediana_d2=0 #
	mediana_nd=0 #

	result_del=` expr $pocet_radku / 2 `
	ostatok=` expr $pocet_radku % 2 `

	for i in $all_file_size
	do
		if [ "$ostatok" -eq "0" ]
			then
				mezivysl=` expr $median_count / $result_del `
				ost_mezivysl=` expr $median_count % $result_del `
				if [ "$mezivysl" -eq "1" ]
					then
					 if [ "$ost_mezivysl" -eq "0" ]
					 	then
					 		mediana_d1=$i
					 fi
				fi

				odecitani=$(($median_count-$result_del))
				if [ "$odecitani" -eq "1" ]
					then
						mediana_d2=$i
				fi
				median_count=$(($median_count+1))
		fi #konec ostatok-eq0

		if [ "$ostatok" -eq "1" ]
			then
				mezivysl=$(($median_count-$result_del))
					if [ "$mezivysl" -eq "1" ]
						then 
							mediana_nd=$i
							break
						else
							median_count=$(($median_count+1))
					fi
		fi #konec -eq1 

	done

	if [ "$ostatok" -eq "0" ]
		then
			if [ "$all_files" -eq "0" ]
				then
					echo "  Filze size median: N/A"
				else
					mediana=$(($mediana_d2+$mediana_d1))
					echo "  File size median: `expr $mediana / 2 `"
			fi
	fi

	if [ "$ostatok" -eq "1" ]
		then
			if [ "$all_files" -eq "0" ]
				then
					echo "  File size median: N/A"
				else
					mediana=$mediana_nd
					echo "  File size median: $mediana"
			fi
	fi
	
#9)Work with suffix
p_suf=`find -type f ! -name "$file_ere" -printf "%f\n" | cut -c 2- | sed -ne 's/^.*\.\([^/]*\)$/\1/p'|sort|uniq|wc -l` #zjistit, ze v adresari existuje alespon jeden soubor s sufixem
prip=`find -type f ! -name "$file_ere" -printf "%f\n" | cut -c 2- | sed -ne 's/^.*\.\([^/]*\)$/\1/p'|sort|uniq`
c_all=`find -type f ! -name "$file_ere" -printf "%f\n" | cut -c 2- | sed -ne 's/^.*\.\([^/]*\)$/\1/p'|sort|uniq|wc -l`
c_start=1

if [ "$p_suf" -eq "0" ] 
	then
		exit 0
fi

printf "File extensions: "

for i in $prip
do
	test_1=`echo $i|sed s/[a-zA-Z0-9]//g` #pokud suffix obsahuje neco krome alphanumerickych znaku
		if [ -n "$test_1" ]
			then
				continue
		fi                                #konec testu alpha-numerickych znaku

	printf "$i"

	if [ "$c_start" -lt "$c_all" ] 
		then
			printf ","
	fi
	c_start=$(($c_start+1))
done
echo ""

#10)work with file.suffix
for i in $prip
	do
		test_2=`echo $i|sed s/[a-zA-Z0-9]//g` #pokud suffix obsahuje neco krome alphanumerickych znaku
		if [ -n "$test_2" ]
			then
				continue
		fi            						  #konec testu alpha-numerickych znaku

		count_soub=0
		souc_count_soub=0

		#pocet souboru s soucasnym suf
		for q in $seznam_dir
		do
			souc_count_soub=`find $q -maxdepth 1 ! -name "$file_ere" -name "*.$i" -type f |wc -l`
			count_soub=$(($count_soub+$souc_count_soub))
		done

		if [ $count_soub -eq 0 ]
			then
				continue
		fi

		echo "Files .$i: $count_soub"

		#maximalni velikostsouboru s soucasnym suf
		max_file_size_suf=0
		for q in $seznam_dir
		do
			souc_mfs_suf=`find $q -maxdepth 1 ! -name "$file_ere" -name "*.$i" -type f -printf "%s\n" | sort -nr | sed -n 1p` #hledani nejvetsiho souboru v adresari
			if [ -z "$souc_mfs_suf" ]
				then
					continue
			fi

			if [ "$souc_mfs_suf" -gt "$max_file_size_suf" ]
				then 
					max_file_size_suf=$souc_mfs_suf
			fi
		done
		echo "  Largest file .$i: $max_file_size_suf"
		
		#Sum souboru s soucasnym suf
		sum_file_size_suf=0 #sum vseh  rozsahu souboru s sufixem
		cur_sum_file_size_suf=0

		for q in $seznam_dir
			do
				if [ "$count_soub" -eq 0 ]
					then
						continue
				fi
				cur_sum_file_size_suf=`find $q -maxdepth 1 ! -name "$file_ere" -name "*.$i" -type f -ls | awk '{sum = sum+$7};END{print sum}'` #sum vsech rozsahu souboru ve vsech adresarich
				if [ -z "$cur_sum_file_size_suf" ]
					then
						continue
				fi
				sum_file_size_suf=$(($sum_file_size_suf+$cur_sum_file_size_suf))
			done
	    aver_suf=`expr $sum_file_size_suf / $count_soub`
		echo "  Average file size .$i: $aver_suf"
		#mediana souboru s soucasnym suf
		result_del_suf=0
		
		ostatok_suf=0
		ost_mezivysl_suf=0
		odecitani_suf=0

		mezivysl_suf=0
		mezivysl_suf_n=0
		
		median_count_suf=1 #counter
		mediana_d1_suf=0   #1 hodnota pro hledani mediany (ost_suf -eq 0)
		mediana_d2_suf=0   #2 hodnota pro hledani mediany (ost_suf -eq 0)
		mediana_nd_suf=0   #hodnota pro hledani mediany (ost_suf -eq 1)
		mediana_n=0        #hodnota pro hledani mediany (res_deleni -eq 0)
		mediana_suf=0 	   #konecna mediana pro suf


		size_su=`find . ! -name "$file_ere" -name "*.$i" -type f -printf "%s\n" | sort -nr`
		pocet_radku_suf=`find . ! -name "$file_ere" -name "*.$i" -type f -printf "%s\n" | sort -nr | wc -l`
		result_del_suf=`expr $pocet_radku_suf / 2`
		ostatok_suf=`expr $pocet_radku_suf % 2`

		for p in $size_su
			do
				if [ "$result_del_suf" -eq "0" ]
					then
						mediana_n=$size_su
				fi

				if [ "$result_del_suf" -ne "0" ]
					then
						if [ "$ostatok_suf" -eq "0" ]
							then
								mezivysl_suf=`expr $median_count_suf / $result_del_suf`
								ost_mezivysl_suf=`expr $median_count_suf % $result_del_suf`
									if [ "$mezivysl_suf" -eq "1" ]
										then
					 						if [ "$ost_mezivysl_suf" -eq "0" ]
					 							then
					 								mediana_d1_suf=$p

					 						fi
					 				fi
						 

							odecitani_suf=$(($median_count_suf - $result_del_suf))
							if [ "$odecitani_suf" -eq "1" ]
								then
									mediana_d2_suf=$p
							fi
							median_count_suf=$(($median_count_suf + 1))
						fi #konec ostatok-eq0

						if [ "$ostatok_suf" -eq "1" ]
							then
								mezivysl_suf_n=$(($median_count_suf-$result_del_suf))
									if [ "$mezivysl_suf_n" -eq "1" ]
										then 
											mediana_nd_suf=$p
											break
										else
											median_count_suf=$(($median_count_suf+1))
									fi
						fi #konec -eq1 
				fi
		done

	  if [ "$result_del_suf" -ne "0" ]
	  	then
			if [ "$ostatok_suf" -eq "0" ]
				then
					if [ "$pocet_radku_suf" -eq "0" ]
						then
							echo "  File size median: N/A"
						else
							mediana_suf=$(($mediana_d2_suf+$mediana_d1_suf))
							echo "  File size median .$i: `expr $mediana_suf / 2`"
					fi
			fi

			if [ "$ostatok_suf" -eq "1" ]
				then
					if [ "$pocet_radku_suf" -eq "0" ]
						then
							echo "  Filze size median: N/A"
						else
							echo "  File size median .$i: $mediana_nd_suf"
					fi
			fi
	fi

	if [ "$result_del_suf" -eq "0" ]
		then
			if [ "$pocet_radku_suf" -eq "0" ]
				then
					echo "  File size median: N/A"
				else
					echo "  File size median .$i: $mediana_n"
			fi
	fi
	done
}

#WORK WITH ARGUMENTS

posun=0
file_ere=""

if [ "$#" -gt "3" ]
	then
		print_help
fi

while getopts ":i:" option 
do
	case $option in

		i)	file_ere=$OPTARG
    		posun=$(($posun+1)) 		#nastaveni argumentu prepinace do promene
			;;
			
		*) echo " Script doesn't work with this key" #pripad spatneho prepinace
		   print_help
		  	;;
	esac
done

if [ "$posun" -eq "1" ] 
	then
		shift 2
fi

if [ -z "$1" ] 				#adresar pro test. Pokud nenalezen, skanujeme puvodni adresar
	then
		orig=`pwd`
	else
		if [ -d "$1" ] 		#pokud mame cestu do adresare a adresar existuje
			then
				orig=$1 
				cd "$orig"
			else
				print_help
		fi
fi



Dir_work

exit 0
